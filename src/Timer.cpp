#include <chrono>
#include <thread>

struct Timer
{
private:
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
public:
    Timer() : start(std::chrono::high_resolution_clock::now()) {}
    
    ~Timer()
    {
            end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<float> duration = end - start;
            std::cout << duration.count() << "s " << std::endl;
    }
};

#if 0
void Function()
{
    Timer timer;
    for (int i =0 ; i < 100; i++)
        std::cout << "hello\n";
}

int main() 
{
    Function();
    
    std::cin.get();
}
#endif