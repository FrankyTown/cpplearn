#include "Entity.cpp"

class Player : public Entity
{
public:
    int m_Level = 0;

    Player(): Entity() {}
    Player(std::string Name)
        : Entity(Name) {}
    
    void LevelUp() { m_Level++; }
};