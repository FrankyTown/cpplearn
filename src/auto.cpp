#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

class Device 
{
public:
    void DoesNothing() const {}
};

class DeviceManager
{
private:
    std::unordered_map<std::string, std::vector<Device*>> m_Devices;
public:
    const std::unordered_map<std::string, std::vector<Device*>>& GetDevices() const
    {
        return m_Devices;
    }
};

//int main() 
//{
//    std::vector<std::string> strings;
//    strings.emplace_back("Apple");
//    strings.emplace_back("Orange");
//    
//    for (/*std::vector<std::string>::iterator*/ auto it = strings.begin();
//        it != strings.end(); it++)
//            std::cout << *it << std::endl;
//
//    //using DeviceMap = std::unordered_map<std::string, std::vector<Device*>>;
//    
//    DeviceManager dm;
//    const auto& devices = dm.GetDevices();
//    
//    devices.size();
//    std::cin.get();
//}