#include <iostream>
#include <string>
#include <algorithm>

namespace apple { namespace functions {
    void print(const std::string& text)
    {
        std::cout << text << std::endl;
    }
}}

namespace orange {
    void print(const char* text)
    {
        std::string temp = text;
        std::reverse(temp.begin(), temp.end());
        std::cout << temp << std::endl;
    }
}
#if 0
int main() 
{
    namespace af = apple::functions;
    af::print("hello");
    orange::print("hello");

    std::cin.get();
}

#endif