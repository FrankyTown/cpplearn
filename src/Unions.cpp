#include <iostream>

struct Vector2
{
    float x, y;
};

struct Vector4
{// is actually two Vector2
    union
    {
        struct
        {
            float x, y, z, w;
        };
        struct // Since unions can only have one member, this second struct
               // occupies the same memory as the previous one.
        {
            Vector2 a, b;
        };
    };
};

struct TPVector4 // Using type punning
{
    float x, y, z, w;
    
    Vector2& GetA()
    {
        return *(Vector2*)&x;
    }
    Vector2& GetB()
    {
        return *(Vector2*)&z;
    }
};

void PrintVector2(const Vector2& vector)
{
    std::cout << vector.x << ", " << vector.y << std::endl;
}

#if 0
int main() 
{
    Vector4 vector = {1.0f, 2.0f, 3.0f, 4.0f};
    vector.x = 2.0f;
    
    PrintVector2(vector.a);
    PrintVector2(vector.b);
    vector.z = 500.0f;
    std::cout << "-------------------------------------------­\n";
    PrintVector2(vector.a);
    PrintVector2(vector.b);
    
    
    std::cin.get();
}
#endif