#include <iostream>

class Entity
{
public:
    std::string m_Name = "NoName";
    int m_X = 0, m_Y =0;
    bool m_IsDead = false;
    
    Entity()
    {
        std::cout << "Entity is created" << std::endl;
    }
    
    Entity(std::string Name) : m_Name(Name)
    {
        std::cout << "Entity named " << m_Name << " is created" << std::endl;
    }
    
    Entity(int x, int y) : m_X(x), m_Y(y) {}    
    virtual~Entity()
    {
        std::cout << "Entity is deleted" << std::endl;
    }
    
    void Die() {
        m_IsDead = true;
        std::cout << m_Name << " died!" << std::endl;
    }
 
    void Move1() {
        m_X++;
        m_Y++;
        std::cout << m_Name << " move to " << "x:" << m_X << ",y:" << m_Y << std::endl;
    }
    int* GetPosition()
    {// Get pointer to the address of x.
        return &m_X; 
    }
};