#include <iostream>
#include <vector>
#include <algorithm>

struct Vertex 
{
    float x, y, z;
    Vertex(float x, float y, float z): x(x), y(y), z(z) {}
    
    Vertex(const Vertex& Other) : x(Other.x), y(Other.y), z(Other.z) 
    {
        std::cout << "Copied!" << std::endl;
    }
};

void Function(const std::vector<Vertex>& vertices)
{
    // definitely pass std::vector by reference!
}

std::ostream& operator<<(std::ostream& stream, const Vertex& vertex)
{
    return stream << vertex.x << ", " << vertex.y << ", " << vertex.z;
}

#if 0
int main()
{
    //No copy!
    std::vector<Vertex> vertices;
    vertices.reserve(3);
    vertices.emplace_back(1, 2, 3);
    vertices.emplace_back(4, 5, 6);
    vertices.emplace_back(7, 8, 9);

    // Sort!
    std::vector<int> values = {5, 234, 2, 2, 1, 64};
    //std::sort(values.begin(), values.end(), std::greater<int>());
    std::sort(values.begin(), values.end(), [](int a, int b) { return a < b; });

    for (int value : values)
        std::cout << value << std::endl;
    std::cin.get();
}
#endif