#include <iostream>
#include "Entity.cpp"

#if 0
int main() 
{
//    int a =50;
//    double& value = *(double*)&a;

    Entity e = Entity(4, 8);
    
//    int* position = (int*)&e;
//    std::cout << position[0] << ", " << position[1] << std::endl;
    
    //int y = e.y;
    // also
    //int y = *(int*)((char*)&e + 4);
    //std::cout << y << std::endl;
    
    int* position  = e.GetPosition();
    // Nothing stopping me from accessing data that is not mine.
    std::cout << position[0] << ", " << position[1] << ", " << position[2] << std::endl;
    
    std::cin.get();
}
#endif