#include <iostream>

class Base
{
public:
    Base() { std::cout << "Base constructor Base\n"; }
    virtual ~Base() { std::cout << "Base destructor Base\n"; }
};

class Derived : public Base
{
public:
    Derived() { std::cout << "Derived constructor Base\n"; }
    ~Derived() { std::cout << "Derived destructor Base\n"; }
};

int main() 
{
    Base* base = new Base();
    delete base;
    std::cout << "-----------Derived-------------------------­\n";
    Derived* derived = new Derived();
    delete derived;
    std::cout << "-Polymorfism-will-not-call-base-destructor-if-base-desctrutor-is-not-virtual­\n";
    Base* poly = new Derived();
    delete poly;
    
    std::cin.get();
}