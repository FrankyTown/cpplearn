#include <vector>
#include <functional>
#include <algorithm>

void ForEach(const std::vector<int>& values, const std::function<void(int)>& func)
{
    for (int value : values)
        if (value > 3)
            func(value);
}

//int main() 
//{
//    std::vector<int> values = {1, 123, 3452, 2, 54, 2};
//    auto it = std::find_if(values.begin(), values.end(), [](int value) {return value > 3;});
//    std::cout << *it << std::endl;
//    
//    auto lambda = [](int value) {std::cout << "Value: " << value << std::endl;};
//    ForEach(values, lambda);
//
//    std::cin.get();
//}