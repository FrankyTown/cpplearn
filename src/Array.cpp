template<typename T, int N>
class Array
{
private:
    // Templates used to determine size at compile time
    T m_Array[N];
public:
    int GetSize() const { return N; }
};

//int main() 
//{
//    Array<int, 5> array;
//    
//    std::cout << array.GetSize() << std::endl;
//    std::cin.get();
//}