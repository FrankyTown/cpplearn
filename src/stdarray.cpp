#include <iostream>
#include <array>

template<typename T, size_t size>
void PrintArray(std::array<T, size>& data)
{
    for (int i = 0; i < (int)data.size(); i++)
        std::cout << data[i] << std::endl;
}

#if 0
int main() 
{
    std::array<int, 5> data;
    data[0] = 2;
    data[4] = 1;
    
    PrintArray(data);
    
    std::cin.get();
}
#endif