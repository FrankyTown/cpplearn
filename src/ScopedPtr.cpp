template<class T>
class ScopedPtr
{
private:
    T* m_Ptr;
    
public:
    ScopedPtr(T* ptr) : m_Ptr(ptr) {}
    
    ~ScopedPtr()
    {
        delete m_Ptr;
    }
    
    T* operator->()
    {
        return m_Ptr;
    }
    
    const T* operator->() const
    {
        return m_Ptr;
    }
};