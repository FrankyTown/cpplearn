#include <iostream>
#include <vector>

void Hello(int a)
{
    std::cout << "Hello! Value: " << a << std::endl;
}

void PrintValue(int Value)
{
    std::cout << "Value: " << Value << std::endl;
}

void ForEach(const std::vector<int>& values, void(*func)(int))
{
    for (int value : values)
        func(value);
}

//int main() 
//{
//    void(*function)(int a) = Hello;
//    
//    auto function = Hello;
    
//    typedef void(*HelloFct)(int a);
//    HelloFct function = Hello;
//    
//    function(5);
//
//    std::vector<int> values = {1, 2, 12, 6, 345, 23};
//    ForEach(values, PrintValue);
//    
//    //Using lambda fct
//    ForEach(values, [](int value) {std::cout << "Value: " << value << std::endl;});
//    
//    std::cin.get();
//}